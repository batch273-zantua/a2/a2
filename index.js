//without the use of objects, our students from before would be organized as follows if we are to record additional information about them

// //create student one
// let studentOneName = 'John';
// let studentOneEmail = 'john@mail.com';
// let studentOneGrades = [89, 84, 78, 88];

// //create student two
// let studentTwoName = 'Joe';
// let studentTwoEmail = 'joe@mail.com';
// let studentTwoGrades = [78, 82, 79, 85];

// //create student three
// let studentThreeName = 'Jane';
// let studentThreeEmail = 'jane@mail.com';
// let studentThreeGrades = [87, 89, 91, 93];

// //create student four
// let studentFourName = 'Jessie';
// let studentFourEmail = 'jessie@mail.com';
// let studentFourGrades = [91, 89, 92, 93];

// //actions that students may perform will be lumped together
// function login(email){
//     console.log(`${email} has logged in`);
// }

// function logout(email){
//     console.log(`${email} has logged out`);
// }

// function listGrades(grades){
//     grades.forEach(grade => {
//         console.log(grade);
//     })
// }

//This way of organizing employees is not well organized at all.
//This will become unmanageable when we add more employees or functions
//To remedy this, we will create objects

//1. spaghetti
//2. inside {}
//3. OOP
//4. StudentOne.eroll()
//5. true
//6. key: value
//7. true
//8. true
//9. true
//10. true


let studentOne = {
    name:  'John',
    email:  'john@mail.com',
    grades:  [89, 84, 78, 88],

    getAverage(){
        var total = 0;
        for(var i = 0; i < this.grades.length; i++) {
            total += this.grades[i];
        }
        var avg = total / this.grades.length;

        return avg;
        
    },

    willPass(){
        if(this.getAverage() >=85){
            return true;

        } else {
            return false;
        }
    },

    willPassWithHonors(){
       if (this.willPass() && this.getAverage() >= 90){
            return true;
        } else {
            return false;
        }
    }

}

let studentTwo = {
    name:  'Joe',
    email:  'joe@mail.com',
    grades:  [78, 82, 79, 85],

    getAverage(){
        var total = 0;
        for(var i = 0; i < this.grades.length; i++) {
            total += this.grades[i];
        }
        var avg = total / this.grades.length;

        return avg;
        
    },

    willPass(){
        if(this.getAverage() >=85){
            return true;

        } else {
            return false;
        }
    },

    willPassWithHonors(){
       if (this.willPass() && this.getAverage() >= 90){
            return true;
        } else {
            return false;
        }
    }

}
let studentThree = {
    name:  'Jane',
    email:  'jane@mail.com',
    grades:  [87, 89, 91, 93],

    getAverage(){
        var total = 0;
        for(var i = 0; i < this.grades.length; i++) {
            total += this.grades[i];
        }
        var avg = total / this.grades.length;

        return avg;
        
    },

    willPass(){
        if(this.getAverage() >=85){
            return true;

        } else {
            return false;
        }
    },

    willPassWithHonors(){
       if (this.willPass() && this.getAverage() >= 90){
            return true;
        } else {
            return false;
        }
    }

}
let studentFour = {
    name:  'Jessie',
    email:  'jessie@mail.com',
    grades:  [91, 89, 92, 93],

    getAverage(){
        var total = 0;
        for(var i = 0; i < this.grades.length; i++) {
            total += this.grades[i];
        }
        var avg = total / this.grades.length;

        return avg;
        
    },

    willPass(){
        if(this.getAverage() >=85){
            return true;

        } else {
            return false;
        }
    },

    willPassWithHonors(){
       if (this.willPass() && this.getAverage() >= 90){
            return true;
        } else {
            return false;
        }
    }

}

let classOf1A = {
    students: [studentOne, studentTwo, studentThree, studentFour],

    countHonorStudents(){
        var honorStudents = 0;
        for(var i = 0; i < this.students.length; i++) {
            if (this.students[i].willPassWithHonors() == true){
                honorStudents += 1;
            }
        }
        return honorStudents;
    },

    honorsPercentage(){
        return (this.countHonorStudents()/this.students.length) * 100;
    },

    retrieveHonorStudentsInfo(){
        let honorStudentsList =[];
        for(var i = 0; i < this.students.length; i++) {
            if (this.students[i].willPassWithHonors() == true){
                honorStudentsList.push({aveGrade: this.students[i].getAverage(),
                                    email: this.students[i].email});
            }
        }
        return honorStudentsList;

    },

    sortHonorStudentsByGradeDesc(){
        let hStudentsList = this.retrieveHonorStudentsInfo();
        hStudentsList.sort((a, b) => {
            return a.aveGrade - b.aveGrade;
        });
        hStudentsList.reverse();
        return hStudentsList;
    }



}




